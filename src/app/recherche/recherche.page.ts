import { Component, OnInit, ViewChild } from '@angular/core';
import { IonSearchbar } from '@ionic/angular';

@Component({
  selector: 'app-recherche',
  templateUrl: './recherche.page.html',
  styleUrls: ['./recherche.page.scss'],
})
export class RecherchePage implements OnInit {

  @ViewChild('search', { static: false}) search: IonSearchbar;

  public list: Array<Object> = [];
  private searchedItem: any;

  constructor() {
    this.list = [
      { title: "iron man" },
      { title: "harry potter" }
    ];
   }

  ngOnInit() {
  }
  ionViewDidEnter(){
    setTimeout(() => {
      this.search.setFocus();
    })
  }

  _ionChange(event){
    const val = event.target.value;
    this.searchedItem = this.list;
    if (val && val.trim() != ''){
      this.searchedItem = this.searchedItem.filter((item: any) => {
        return (item.title.toLowerCase().indexOf(val.toLowerCase()) > -1);
      })
    }
    console.log(event.detail.value)
  }
}
