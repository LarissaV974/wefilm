import { Component } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { FilmPage } from '../film/film.page';

@Component({
  selector: 'app-tab1',
  templateUrl: 'tab1.page.html',
  styleUrls: ['tab1.page.scss']
})
export class Tab1Page {

  constructor(
    public modalController: ModalController
  ) {}
  async openFilm() {
    const modal = await this.modalController.create({
      component: FilmPage
    });
    return await modal.present();
  }

}
