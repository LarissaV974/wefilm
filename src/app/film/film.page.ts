import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';


@Component({
  selector: 'app-film',
  templateUrl: './film.page.html',
  styleUrls: ['./film.page.scss'],
})
export class FilmPage implements OnInit {

  constructor(
    public modalController: ModalController
  ) { }
 
  close() {
    this.modalController.dismiss(); //fermer le modal controller
  }

  ngOnInit() {
  }

}
