import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { ActeurPage } from './acteur.page';

describe('ActeurPage', () => {
  let component: ActeurPage;
  let fixture: ComponentFixture<ActeurPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ActeurPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(ActeurPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
