import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ActeurPage } from './acteur.page';

const routes: Routes = [
  {
    path: '',
    component: ActeurPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ActeurPageRoutingModule {}
